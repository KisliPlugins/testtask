﻿using System.Collections.Generic;

namespace DataProvider.Base
{
    public interface IRowData
    {
        RowType RowType{ get; }
        Dictionary<string, string> RowData{ get; }
        string SectionName{ get; set; }
        string ClassName1{ get; set; }
        string ClassName2{ get; set; }
        string ClassCode1{ get; set; }
        string ClassCode2{ get; set; }
    }

    public enum RowType
    {
        Header = 0,
        Data = 1
    }
}