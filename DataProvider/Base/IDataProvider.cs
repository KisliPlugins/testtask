﻿using System.Collections.Generic;

namespace DataProvider.Base
{
    public interface IDataProvider
    {
        List<List<IRowData>> LoadData();
    }
}