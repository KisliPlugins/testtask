﻿using System;
using System.Collections.Generic;
using Microsoft.Office.Interop.Excel;

namespace DataProvider.Excel
{
    public class SheetsData
    {
        public List<List<string>> DataByRows{ get; set; }
        public string SheetName{ get; set; }

        public override string ToString(){
            var str = SheetName + Environment.NewLine;
            foreach (var row in DataByRows){
                str += string.Join("\t", row) + Environment.NewLine;
            }

            return str;
        }


        public SheetsData(Worksheet excelworksheet){
            DataByRows = new List<List<string>>();
            SheetName = excelworksheet.Name;
            var excelcells = excelworksheet.UsedRange;
            object[,] worksheetValuesArray = excelcells.get_Value(Type.Missing);
            for (var row = 1; row < worksheetValuesArray.GetLength(0) + 1; row++){
                var newRow = GetRowData(worksheetValuesArray, row);

                DataByRows.Add(newRow);
            }
        }

        private static List<string> GetRowData(object[,] worksheetValuesArray, int row){
            var newRow = new List<string>();
            for (var col = 1; col < worksheetValuesArray.GetLength(1) + 1; col++){
                var cell = worksheetValuesArray[row, col];

                GetCellContent(cell, newRow);
            }

            return newRow;
        }

        private static void GetCellContent(object cell, List<string> newRow){
            if (cell == null){
                newRow.Add(" ");
            }
            else{
                newRow.Add(cell.ToString());
            }
        }

        
    }
}