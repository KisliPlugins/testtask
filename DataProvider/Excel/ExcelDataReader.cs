﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices;
using System.Windows.Forms;
using Helpers.Extensions;
using Microsoft.Office.Interop.Excel;
using Application = Microsoft.Office.Interop.Excel.Application;

namespace DataProvider.Excel
{
    public class ExcelDataReader
    {
        public string PathToFile{ get; }
        public List<SheetsData> Sheets{ get; }

        public ExcelDataReader(string pathToFile){
            Sheets = new List<SheetsData>();
            PathToFile = pathToFile;
            ReadDataAndFillSheetsData();
        }


        private void ReadDataAndFillSheetsData(){
            Application excelapp = null;
            var fileInfo = new FileInfo(PathToFile);
            var isFileOpened = IsFileOpened(fileInfo);

            if (isFileOpened){
                //if file is busy then we try fins excel app where this file using
                excelapp = Marshal.GetActiveObject("Excel.Application") as Application;
            }
            else{
                excelapp = new Application();
                excelapp.Visible = false;
            }

            var excelAppWorkbook = GetWorkBook(excelapp);

            FillSheetsData(excelAppWorkbook);

            if (!isFileOpened){
                excelAppWorkbook.Close(false, Type.Missing, Type.Missing);
                excelapp.Quit();
            }
        }

        private Workbook GetWorkBook(Application excelapp){
            Workbook excelappworkbook = null;

            excelappworkbook = excelapp.Workbooks.ToList<Workbook>().FirstOrDefault(x => x.Path.Equals(PathToFile));

            if (excelappworkbook == null){
                excelappworkbook = excelapp.Workbooks.Open(
                    PathToFile,
                    Type.Missing, Type.Missing, true, Type.Missing,
                    Type.Missing, Type.Missing, Type.Missing, Type.Missing,
                    Type.Missing, Type.Missing, Type.Missing, Type.Missing,
                    Type.Missing, Type.Missing);
            }

            return excelappworkbook;
        }

        private void FillSheetsData(Workbook book){
            var allSheets = book.Sheets.ToList<Worksheet>();

            foreach (var worksheet in allSheets){
                Sheets.Add(new SheetsData(worksheet));
            }
        }

        private bool IsFileOpened(FileInfo fileInfo){
            try{
                using (var stream = fileInfo.Open(FileMode.Open, FileAccess.ReadWrite, FileShare.None)){
                    stream.Close();
                }
            }
            catch (IOException){
                //the file is unavailable because it is:
                //still being written to
                //or being processed by another thread
                //or does not exist (has already been processed)
                return true;
            }

            //file is not locked
            return false;
        }

        public static string GetPathToFile(string ext = "Excel Files(*.XLS;*.XLSX )|*.XLS;*.XLSX"){
            var path = "";
            using (var OPF = new OpenFileDialog()){
                OPF.Filter = string.Format(ext);

                if (OPF.ShowDialog() == DialogResult.OK){
                    path = OPF.FileName;
                }
            }

            return path;
        }

        public override string ToString(){
            return string.Join("\n===============================================\n", Sheets);
        }
    }
}