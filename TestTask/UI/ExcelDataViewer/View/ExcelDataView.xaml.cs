﻿using System.Windows;
using TestTask.UI.ExcelDataViewer.ViewModel;

namespace TestTask.UI.ExcelDataViewer.View
{
    /// <summary>
    ///     Interaction logic for ExcelDataView.xaml
    /// </summary>
    public partial class ExcelDataView : Window
    {
        public ExcelDataView(){
            InitializeComponent();
            DataContext = new ExcelDataViewModel();
        }
    }
}