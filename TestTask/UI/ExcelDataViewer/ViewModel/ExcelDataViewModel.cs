﻿using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Input;
using DataProvider.Base;
using DataProvider.Excel;
using Prism.Commands;
using TestTask.UI.ExcelDataViewer.Data;

namespace TestTask.UI.ExcelDataViewer.ViewModel
{
    public class ExcelDataViewModel : BaseViewModel
    {
        private ObservableCollection<IRowData> _datas;

        public IDataProvider DataProvider{ get; private set; }

        public ObservableCollection<IRowData> Datas{
            get{ return _datas; }
            private set{
                _datas = value;
                OnPropertyChanged();
            }
        }

        public IRowData Header{ get; private set; }
        public List<IRowData> AllData{ get; private set; }
        public ICommand OpenExcel{ get; set; }
        public MainWindow Wn{ get; set; }

        public ExcelDataViewModel(){
            OpenExcel = new DelegateCommand(OnOpenExcel);
        }

        private void OnOpenExcel(){
            var pathToFile = ExcelDataReader.GetPathToFile();

            DataProvider = new ExcelDataProvider(pathToFile);
            AllData = new List<IRowData>(DataProvider.LoadData().SelectMany(x => x));

            Header = AllData.Where(x => x.RowType == RowType.Header).OrderBy(x => x.RowData.Count).Last();

            Datas = new ObservableCollection<IRowData>(AllData.Where(x => x.RowType == RowType.Data));
            LoadDataGrid();
        }

        private void LoadDataGrid(){
            var dataGrid = Wn.DataGrid;
            //try fill data and create columns dynamicaly but i not found solution ho doing data binding
            foreach (var header in Header.RowData.Keys){
                var gridColumn = new DataGridTextColumn{Header = header, IsReadOnly = true};
                SetBinding(gridColumn, header);
                dataGrid.Columns.Add(gridColumn);
            }
        }
        private void SetBinding(DataGridTextColumn gridColumn, string header)
        {
            Binding myBinding = new Binding();
            myBinding.Source = Datas;
            myBinding.Path = new PropertyPath("ClassName1");
            myBinding.Mode = BindingMode.TwoWay;
            myBinding.UpdateSourceTrigger = UpdateSourceTrigger.PropertyChanged;
            BindingOperations.SetBinding(gridColumn, TextBox.TextProperty, myBinding);
        }
    }
}