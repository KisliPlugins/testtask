﻿using System.Collections.Generic;
using DataProvider.Base;
using DataProvider.Excel;

namespace TestTask.UI.ExcelDataViewer.Data
{
    public class ExcelDataProvider : IDataProvider
    {
        private readonly string _pathToExcel;

        public ExcelDataProvider(string pathToExcel){
            _pathToExcel = pathToExcel;
        }

        private static void AddDataToRow(SheetsData sheet, List<IRowData> sheetData, List<string> header){
            for (var i = 1; i < sheet.DataByRows.Count; i++){
                sheetData.Add(new TableRowData(sheet.DataByRows[i], header, RowType.Data));
            }
        }

        public List<List<IRowData>> LoadData(){
            var newExcelReader = new ExcelDataReader(_pathToExcel);
            var dataRows = new List<List<IRowData>>();

            foreach (var sheet in newExcelReader.Sheets){
                var sheetData = new List<IRowData>();
                var header = sheet.DataByRows[0];
                sheetData.Add(new TableRowData(header, header, RowType.Header));
                AddDataToRow(sheet, sheetData, header);
                dataRows.Add(sheetData);
            }

            return dataRows;
        }
    }
}