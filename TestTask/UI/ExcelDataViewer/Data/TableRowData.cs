﻿using System.Collections.Generic;
using DataProvider.Base;

namespace TestTask.UI.ExcelDataViewer.Data
{
    public class TableRowData : IRowData
    {
        public TableRowData(List<string> rowData, List<string> header, RowType data){
            RowType = data;
            RowData = new Dictionary<string, string>();
            for (var i = 0; i < header.Count; i++){
                RowData[header[i]] = rowData[i];
                FillProps(i, rowData[i]);
            }
        }

        private void FillProps(int i, string s){
            switch (i){
                case 0:
                    SectionName = s;
                    break;
                case 1:
                    ClassName1 = s;
                    break;
                case 2:
                    ClassCode1 = s;
                    break;
                case 3:
                    ClassName2 = s;
                    break;
                case 4:
                    ClassCode2 = s;
                    break;
            }
        }

        public Dictionary<string, string> RowData{ get; }
        public string SectionName{ get; set; }
        public string ClassName1{ get; set; }
        public string ClassName2{ get; set; }
        public string ClassCode1{ get; set; }
        public string ClassCode2{ get; set; }
        public RowType RowType{ get; }
    }
}