﻿using System.Windows;
using TestTask.UI.ExcelDataViewer.ViewModel;

namespace TestTask
{
    /// <summary>
    ///     Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        #region Constructors

        public ExcelDataViewModel Model{ get; set; }

        public MainWindow(){
            InitializeComponent();
            Model = new ExcelDataViewModel();
            Model.Wn = this;
            DataContext = Model;
        }

        #endregion
    }
}