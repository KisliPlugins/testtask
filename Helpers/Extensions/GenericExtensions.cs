﻿using System.Collections;
using System.Collections.Generic;

namespace Helpers.Extensions
{
    public static class GenericExtensions
    {
        #region Static Methods

        public static List<T> ToList<T>(this object car){
            if (car is IEnumerable enu){
                var retl = new List<T>();
                foreach (T v in enu){
                    retl.Add(v);
                }

                return retl;
            }

            return new List<T>{(T) car};
        }

        #endregion
    }
}